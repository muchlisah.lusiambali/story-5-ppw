from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.

"""
def index(request):
    return render(request,"index.html")
"""

# Story 1
#def story1_index(request):
def index(request):
    return render(request, "story1/index.html")

def description(desc):
    return render(desc, "story1/description.html")

def background(education):
    return render(education, "story1/background.html")

def socialmedia(socmed):
    return render(socmed, "story1/socialmedia.html")
